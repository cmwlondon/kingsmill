/*
	{
		"id" : ,
		"title" : "",
		"description" : "",
		"tags" : "",
		"lat" : ,
		"lng" : ,
		"placename" : "",
		"votes" : 0,
		"fb" : 8817,
		"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
	}
*/
{
	"places" : [
		{
			"id" : 1,
			"title" : "Regents park",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 51.535078,
			"lng" : -0.150933,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 2,
			"title" : "Soho square",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 51.515298,
			"lng" : -0.131994,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 3,
			"title" : "Devonshire arms",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 51.540757,
			"lng" : -0.142052,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 4,
			"title" : "Knighton Wood, Buckhurst Hill",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 51.623385,
			"lng" : 0.038366,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 5,
			"title" : "68 buckingham road",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 51.597774,
			"lng" : 0.018421,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 6,
			"title" : "Norwich city centre",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 52.623112,
			"lng" : 1.294906,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 7,
			"title" : "Slapton Ley",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 50.277932,
			"lng" : -3.651237,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 8,
			"title" : "20 Northumberland St, Plymouth",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 50.398503,
			"lng" : -4.1782,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 9,
			"title" : "Stoke Beach caravan park",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 50.298207,
			"lng" : -4.018617,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 10,
			"title" : "Burrator Reservoir, Dartmoor",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 50.49406,
			"lng" : -4.044667,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 11,
			"title" : "Stonehenge",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 51.178879,
			"lng" : -1.826187,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		},
		{
			"id" : 12,
			"title" : "Hull University campus",
			"description" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"tags" : "",
			"lat" : 53.771392,
			"lng" : -0.368192,
			"placename" : "",
			"votes" : 0,
			"fb" : 8817,
			"images" : ["assets/img/infobox/placeholder.jpg","assets/img/top25/placeholder.jpg"]
		}
	]
}