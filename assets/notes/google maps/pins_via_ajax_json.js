// configure google maps instance
var mapOptions = {
	"center" : ukCentre,
	"zoom" : ukZoom,
	"mapTypeId": google.maps.MapTypeId.ROADMAP,
	"panControl" : true,
	"panControlOptions" : {
		"position" : google.maps.ControlPosition.LEFT_BOTTOM
	},
	"zoomControl" : true,
	"zoomControlOptions" : {
		"style" : google.maps.ZoomControlStyle.DEFAULT,
		"position" : google.maps.ControlPosition.LEFT_BOTTOM
	},
	"mapTypeControl" : false // conceal map type selector
};

// create map instance in page in div#googleMap
var map = new google.maps.Map( document.getElementById("googlePopupMap"), mapOptions );

/* load JSON file containting sample marker location */
var places;
var markerList = new Array();
$.ajax({
	"type" : "GET",
	"url" : "assets/json/places.js",
	"data" : "",
	"dataType" : "text",
	"cache" : false,
	"success" : function( text ){
		lunchspots = $.parseJSON( text );
	},
	"complete" : function(){
		var marker, thisPlace, thisPlaceTitle, thisPlaceLocation, thisPlaceZindex
		// iterates through places, add ing map marker for each one
		for( placeIndex = 0; placeIndex < lunchspots.places.length; placeIndex++){
			thisPlace = lunchspots.places[ placeIndex ];
			thisPlaceTitle = thisPlace[0];
			thisPlaceLocation = new google.maps.LatLng( thisPlace[1], thisPlace[2] );
			thisPlaceZindex = thisPlace[3];

			marker = new google.maps.Marker({
				"position" : thisPlaceLocation,
				"map" : map,
				"title" : thisPlaceTitle,
				"icon" : klsPin,
				"zIndex" : thisPlaceZindex
			});

			markerList.push( marker );
		}
	}
});
