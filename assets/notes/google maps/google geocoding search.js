/*
https://developers.google.com/maps/documentation/geocoding/
http://stackoverflow.com/questions/3998654/creating-a-search-form-for-generating-a-google-map
*/

     var geocoder = new google.maps.Geocoder();

     function geocode () {
        geocoder.geocode({
           'address': document.getElementById('search').value
        }, 
        function(results, status) {
           if(status == google.maps.GeocoderStatus.OK) {
              new google.maps.Marker({
                 position: results[0].geometry.location,
                 map: map
              });
              map.setCenter(results[0].geometry.location);
           }
        });
     }
	 
#enterLunchSpot .intro h1.tellus{
}
#enterLunchSpot .intro h2.localpark{
	color:#45ab58;
}
#enterLunchSpot .intro p.nominate{
	color:#3d9db4;
}
#enterLunchSpot .intro p.nominate a:link,
#enterLunchSpot .intro p.nominate a:visited{
	color:#6b8fc4;
	text-decoration:underline;
	font-style:italic;
}
#enterLunchSpot .intro p.nominate a:hover{
	text-decoration:none;
}
#enterLunchSpot .conditional h2.hereswhat{
	color:#3b69af;
}
#enterLunchSpot .conditional ul li{
	color:#3b69af;
	font-style:italic;
}
#enterLunchSpot .conditional label{
	display:none;
}
#enterLunchSpot .conditional #gSearch{
}
#enterLunchSpot .conditional #gSearchGo
}