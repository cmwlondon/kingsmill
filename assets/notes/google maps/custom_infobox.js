/*
----------------------------------------------------------------------------------------------------------------
custom infoBox test code
----------------------------------------------------------------------------------------------------------------
*/
	if( $('#googleTest1').length != 0 ){

		$('.kib2 .scrollFrame').jScrollPane(
			{
				"verticalDragMinHeight" : 25,
				"verticalDragMaxHeight" : 25
			}
		);
		
		function init() {
			map = new google.maps.Map(document.getElementById('googleTest1'), {
				'zoom': ukZoom,
				'center': ukCentre,
				'mapTypeId': google.maps.MapTypeId.ROADMAP
			});

			for( var pinIndex = 0; pinIndex < testPins.length; pinIndex++){
				var thisPin = testPins[ pinIndex ];

				vPinMarker = new google.maps.Marker({
					"position" : new google.maps.LatLng( thisPin.lat, thisPin.lng ),
					"map" : map,
					"title" : thisPin.title,
					"icon" : klsPin,
					"subtitle" : thisPin.subtitle,
					"description" : thisPin.description,
					"image" : ( thisPin.image != '' ) ? thisPin.image : "assets/img/infobox/logo.gif",
					"fbid" : thisPin.fbid,
					"z-index" : 100
				});
				google.maps.event.addListener(vPinMarker, "click", function(e) {
					map.panTo( this.getPosition() );
					
					currentMarker = this; // store current marker so it can be accesses when the infoBox is closed
					this.setIcon( kls2Pin ); // change for larger icon when infoBox is opened
					var shortText = ellipsis( this.description, 15 );
					var moreLink = ( shortText.clipped ) ? "<a href=\"#\" class=\"infoBoxMore\">More &gt;</a>" : "";
					ib.setOptions({
						"content" : "<article class=\"body\"><img alt=\"\" src=\"" + this.image + "\"><header><h1>" + this.title + "</h1><h2>" + this.subtitle + "</h2></header><div class=\"scrollFrame short\"><p class=\"summary\">" + shortText.text + "</p>" + moreLink + "<div class=\"longText\"><p>" + this.description + "</p></div></div></article>"
					});
					ib.open( map, this ); 
				});
			}

			// Create info window. In content you can pass simple text or html code.
			var infowindow = new google.maps.InfoWindow({
				content: "",
				maxWidth: 10
			});

			var boxText = $('<div></div>');
			boxText
			.addClass('customInfoWindow')
			.html('');
			
			var myOptions1 = {
				content: boxText.html(),
				disableAutoPan: false,
				maxWidth: 0,
				pixelOffset: new google.maps.Size(-190, -150),
				zIndex: null,
				boxStyle: {},
				closeBoxMargin: "-5px -5px -23px 2px",
				closeBoxURL: "assets/img/prizes/close.png",
				infoBoxClearance: new google.maps.Size(1, 1),
				isHidden: false,
				pane: "floatPane",
				enableEventPropagation: false
			};
			var ib = new InfoBox(myOptions1);

			// attaceh 'more' link event once infoBox HTMNL has been added to the DOM
			google.maps.event.addListener(ib, 'domready', function() {
				$(".infoBoxMore").click(function(e){
					e.preventDefault();

					// $('.infoBox').animate({ "height" : "350px" }, 500, function(){});
					$('.infoBox').css({ "height" : "auto" });
					$('.infoBox .scrollFrame')
					.removeClass('short')
					.addClass('long');
					
					$('.infoBox .scrollFrame').jScrollPane(
						{
							"verticalDragMinHeight" : 25,
							"verticalDragMaxHeight" : 25
						}
					);

				});
			});			
			
			google.maps.event.addListener(ib, 'closeclick', function() {
				console.log('closeclick');
				currentMarker.setIcon( klsPin ); // change marker back to standard size
			});			
		}
		
		// Register an event listener to fire when the page finishes loading.
		google.maps.event.addDomListener(window, 'load', init);
	}
