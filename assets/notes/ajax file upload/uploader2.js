	// AJAX file upload handling - for map.html
	var uploader = new plupload.Uploader({
		multi_selection : false, 
		runtimes : 'gears,html5,flash,silverlight,browserplus',
		browse_button : 'selectFile',
		//container: 'container',
		max_file_size : '10mb',
		url : 'assets/ajax_test/upload.php',
		// resize : {width : 320, height : 240, quality : 90},
		flash_swf_url : 'assets/js/library/plupload/plupload.flash.swf',
		silverlight_xap_url : 'assets/js/library/plupload/plupload.silverlight.xap',
		filters : [
			{title : "Image files", extensions : "jpg,gif,png"}
		]
	});
	
	// fires on uploader.init();
	uploader.bind('Init', function(up, params) {
		// console.log( 'init' );
	});
	
	// fires when files are added to queue
	uploader.bind('FilesAdded', function(up, files) {
		// console.log( 'FilesAdded' );
		var thisImage = files[0];
		$('#imageFile').text( thisImage.name );
		/*
		thisImage.size;
		for (var i in files) {}
		*/
		$('.uploadWidget')
		.removeClass('selectState')
		.addClass('uploadState');
	});
	
	// BeforeUpload(uploader:Uploader, file:File)
	uploader.bind('BeforeUpload', function(up, file) {
		// console.log( 'BeforeUpload' );
	});
	
	// UploadFile(uploader:Uploader, file:File)
	uploader.bind('UploadFile', function(up, file) {
		// console.log( 'UploadFile' );
	});
	
	// fies as files are uploaded
	uploader.bind('UploadProgress', function(up, file) {
		// console.log(file.percent);
		var progressBar = Math.floor( file.percent * 182 );
		$('#imageProgress').css({ "width" : progressBar + "px" });
	});

	uploader.bind('FileUploaded', function(up, file, r) {
		console.log( r );
		var fileUploads = $.parseJSON( r.response );
		console.log( fileUploads.filename );
	});
	
	// fires when all files in queue have been uploaded
	uploader.bind('UploadComplete', function(up, file) {
		// console.log( 'UploadComplete' );
		$('.uploadWidget')
		.removeClass('uploadState')
		.addClass('endState');
	});
	
	// fires when user clicks on 'upload' button - 
	$('#uploadFile').click(function(e){
		e.preventDefault();
		uploader.start();
	});
	
	// uploader.init();
