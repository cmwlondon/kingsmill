	public function upload(){
		$post = $_POST;
		$fileObject = $_FILES;
		$data["type"] = $fileObject["file"]["type"];
		$data["original"] = $fileObject["file"]["name"];
		$data["filesize"] = $fileObject["file"]["size"];
		
		switch( $data["type"] ){
			case "application/pdf" : { $fileExtension = "pdf"; } break;
			case "application/msword" : { $fileExtension = "doc"; } break;
			case "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : { $fileExtension = "docx"; } break;
			case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" : { $fileExtension = "xlsx"; } break;
			case "image/jpeg" : { $fileExtension = "jpg"; } break;
			case "image/bmp" : { $fileExtension = "bmp"; } break;
			case "image/png" : { $fileExtension = "png"; } break;
			case "image/gif" : { $fileExtension = "gif"; } break;
			case "text/plain" : { $fileExtension = "txt"; } break;
			default : {
				if( preg_match("/\.([a-zA-Z0-9]{3,4})$/", $data["original"], $matches) ){
					$fileExtension = $matches[1];
				};
			} break;
		}
		
		// asve uploaded documents under plupload generated id
		$newImageName = $_POST["docid"].'.'.$fileExtension;
		if(!file_exists( $this->registry['config']['UPLOADS'].$newImageName ) ){
			move_uploaded_file( $fileObject["file"]["tmp_name"], $this->registry['config']['UPLOADS'].$newImageName);
		}
		$data["filename"] = $newImageName;
		
		// generate JSON file to respond to pluploader with
		$json = "{\n";
		$json .= "\"docid\" : \"".$_POST["docid"]."\",\n";
		$json .= "\"original\" : \"".$data["original"]."\",\n";
		$json .= "\"filename\" : \"".$data["filename"]."\",\n";
		$json .= "\"filesize\" : \"".$data["filesize"]."\",\n";
		$json .= "\"type\" : \"".$data["type"]."\",\n";
		$json .= "\"extension\" : \"".$fileExtension."\"\n";
		$json .= "}\n";
		echo $json;
	}
