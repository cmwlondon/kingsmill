/*
<script type="text/javascript" src="[subdir]assets/js/library/plupload_1_5_2/js/plupload.js"></script>
<script type="text/javascript" src="[subdir]assets/js/library/plupload_1_5_2/js/plupload.gears.js"></script>
<script type="text/javascript" src="[subdir]assets/js/library/plupload_1_5_2/js/plupload.silverlight.js"></script>
<script type="text/javascript" src="[subdir]assets/js/library/plupload_1_5_2/js/plupload.flash.js"></script>
<script type="text/javascript" src="[subdir]assets/js/library/plupload_1_5_2/js/plupload.browserplus.js"></script>
<script type="text/javascript" src="[subdir]assets/js/library/plupload_1_5_2/js/plupload.html4.js"></script>
<script type="text/javascript" src="[subdir]assets/js/library/plupload_1_5_2/js/plupload.html5.js"></script>
*/

// <input id="plupload-browse-button" type="button" value="Select Files" class="button" />

/* 	"docUpload" : "[subdir]document/upload"
	public function upload(){
		$post = $_POST;
		$fileObject = $_FILES;
		$data["type"] = $fileObject["file"]["type"];
		$data["original"] = $fileObject["file"]["name"];
		$data["filesize"] = $fileObject["file"]["size"];
		
		switch( $data["type"] ){
			case "application/pdf" : { $fileExtension = "pdf"; } break;
			case "application/msword" : { $fileExtension = "doc"; } break;
			case "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : { $fileExtension = "docx"; } break;
			case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" : { $fileExtension = "xlsx"; } break;
			case "image/jpeg" : { $fileExtension = "jpg"; } break;
			case "image/bmp" : { $fileExtension = "bmp"; } break;
			case "image/png" : { $fileExtension = "png"; } break;
			case "image/gif" : { $fileExtension = "gif"; } break;
			case "text/plain" : { $fileExtension = "txt"; } break;
			default : {
				if( preg_match("/\.([a-zA-Z0-9]{3,4})$/", $data["original"], $matches) ){
					$fileExtension = $matches[1];
				};
			} break;
		}
		
		// asve uploaded documents under plupload generated id
		$newImageName = $_POST["docid"].'.'.$fileExtension;
		if(!file_exists( $this->registry['config']['UPLOADS'].$newImageName ) ){
			move_uploaded_file( $fileObject["file"]["tmp_name"], $this->registry['config']['UPLOADS'].$newImageName);
		}
		$data["filename"] = $newImageName;
		
		// generate JSON file to respond to pluploader with
		$json = "{\n";
		$json .= "\"docid\" : \"".$_POST["docid"]."\",\n";
		$json .= "\"original\" : \"".$data["original"]."\",\n";
		$json .= "\"filename\" : \"".$data["filename"]."\",\n";
		$json .= "\"filesize\" : \"".$data["filesize"]."\",\n";
		$json .= "\"type\" : \"".$data["type"]."\",\n";
		$json .= "\"extension\" : \"".$fileExtension."\"\n";
		$json .= "}\n";
		echo $json;
	}
*/
var ajaxRoutes = {
	"diaryList" : "[subdir]adminDiaryAjax",
	"diaryData" : "[subdir]diary/data/",
	"diaryAdd" : "[subdir]diary/add",
	"diaryUpdate" : "[subdir]diary/update/",
	"diaryDelete" : "[subdir]diary/deleteAjax/",
	"docData" : "[subdir]document/data/",
	"docDelete" : "[subdir]document/deleteAjax/",
	"docUpdate" : "[subdir]document/update/",
	"docUpload" : "[subdir]document/upload"
}

var uploader = new plupload.Uploader({
	"runtimes" : "html5,silverlight,flash,html4",
	"drop_element" : "dragPanel",
	"browse_button" : "plupload-browse-button",
	"multiple_queues" : true,
	"url" : ajaxRoutes.docUpload, // points to server-side code which handles file uploads
	"multipart" : true,
	"urlstream_upload" : true,
	"multipart_params" : {  },
	"flash_swf_url" : siteSubdir + "assets/js/library/plupload_1_5_2/js/plupload.flash.swf",
	"silverlight_xap_url" : siteSubdir + "assets/js/library/plupload_1_5_2/js/plupload.silverlight.xap"
});
 
uploader.bind('Init', function(up) {
});

// fire event when files are dropped into drop box 
uploader.bind('FilesAdded', function(up, files) {
	
	var index,thisFile;
	var pointer = $('.fileQueue li').length;
	for( index in files){
		thisfile = files[index];
		
		fileItemNode = $('<li></li>')
		.attr({ "id" : "doc_" + thisfile['id'] })
		.addClass('doc' + pointer)
		.addClass('unlinked');

		progessNode = $('<div></div>')
		.addClass('progress');

		infoNode = $('<div></div>')
			// open title panel when user clicks on uploaded document
			.click(function(e){
			var thisItem = $(this).parents('li').eq(0);
			var thisIndex = $('ul.fileQueue li').index( thisItem );

			var thisPosition = thisItem.position();
			
			var originalFilename = thisItem.find('input#original' + thisIndex).val();
			$('.titleWrapper .titleOverlay #originalFilename').text( originalFilename );
			$('.titleWrapper .titleOverlay #titleZero').val( thisItem.find('input#title' + thisIndex).val() );
			$('.titleWrapper .titleOverlay')
			.addClass( 'item' + thisIndex )
			.css({"left" : thisPosition.left + "px", "top" : thisPosition.top + "px"})
			.show();
			$('.titleWrapper .titleOverlay #titleZero').focus();
		})
		.addClass('info');
		
		titleNode = $('<p></p>')
		.addClass('title')
		.text( 'Title' )
		;

		typeNode = $('<p></p>')
		.addClass('type')
		.text( 'Type' );

		sizeNode = $('<p></p>')
		.addClass('size')
		.text( Math.floor ( thisfile['size'] / 1024 ) + 'Kb' );

		infoNode
		.append( titleNode )
		.append( typeNode )
		.append( sizeNode )
		;
					
		fileItemNode
		.append( progessNode )
		.append( infoNode );
		
		$('.fileQueue').append( fileItemNode );
		pointer++;
	}
	
	up.refresh(); // refresh upload queue;
	
	// wait for one second then start file uploads
	setTimeout(function() {
		up.start();
	}, 1000);
});

// fire when files are added to upload queue 
uploader.bind('QueueChanged', function(up) {
	// $('#uploader').pluploadQueue().settings.multipart_params = { var1: $S('var1').value, var2: $S('var2').value};
});

uploader.bind('UploadFile', function(up, file) {
	$.extend(up.settings.multipart_params, { "docid" : file.id });
});
	// update upload progress display
uploader.bind('UploadProgress', function(up, file) {
	var item = jQuery('#doc_' + file.id);
	var height = Math.floor(  132 * (file.percent / 100) );
	jQuery('.progress', item).css({"height" : height + 'px' });
	
});

// fires when all files in queue have been uploaded
uploader.bind('UploadComplete', function(up, file) {
});

// fires as each file upload completes
// handles JSON response from upload script containing image dimensions and mimetype
uploader.bind('FileUploaded', function(up, file, response) {
	var dataBlock, dataBlockIndex, docData;
	
	dataBlock = $('#doc_' + file.id);
	dataBlockIndex = $('.fileQueue li').index( dataBlock );
	docData = $.parseJSON( response['response'] );
	dataBlock.find('.type').text(docData.extension);
	
	var formDiv = $('<div></div>')
	.addClass('detailForm');
	
	var hiddendata1 = $('<input></input>')
	.attr({
		"type" : "hidden",
		"name" : "docid" + dataBlockIndex,
		"id" : "docid" + dataBlockIndex
	})
	.val( docData.docid );

	var hiddendata2 = $('<input></input>')
	.attr({
		"type" : "hidden",
		"name" : "type" + dataBlockIndex,
		"id" : "type" + dataBlockIndex
	})
	.val( docData.type );

	var hiddendata3 = $('<input></input>')
	.attr({
		"type" : "hidden",
		"name" : "title" + dataBlockIndex,
		"id" : "title" + dataBlockIndex
	})
	.val( '' );

	var hiddendata4 = $('<input></input>')
	.attr({
		"type" : "hidden",
		"name" : "original" + dataBlockIndex,
		"id" : "original" + dataBlockIndex
	})
	.val( docData.original );

	var hiddendata5 = $('<input></input>')
	.attr({
		"type" : "hidden",
		"name" : "size" + dataBlockIndex,
		"id" : "size" + dataBlockIndex
	})
	.val( docData.filesize );

	var hiddendata5 = $('<input></input>')
	.attr({
		"type" : "hidden",
		"name" : "extension" + dataBlockIndex,
		"id" : "extension" + dataBlockIndex
	})
	.val( docData.extension );

	var hiddendata6 = $('<input></input>')
	.attr({
		"type" : "hidden",
		"name" : "size" + dataBlockIndex,
		"id" : "size" + dataBlockIndex
	})
	.val( docData.filesize );

	formDiv
	.append( hiddendata1 )
	.append( hiddendata2 )
	.append( hiddendata3 )
	.append( hiddendata4 )
	.append( hiddendata5 )
	.append( hiddendata6 )
	;
	
	dataBlock
	.append( formDiv );
});

uploader.bind('ChunkUploaded', function(up, file, response) {
});

uploader.init();
