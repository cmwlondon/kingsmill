<?php
/* server side validation / AJAX submission / response test file */

$errors	= array();
$fields = array();
/*
#forename nonblank [A-Za-z]
#lastname nonblank [A-Za-z]
#address nonblank [A-Za-z0-9\s\,]
#postcode nonblank, valid postcode
#email nonblank, valid email address
cb: #termsAccept
cb: #ageVerify
*/
if( $_POST && array_key_exists('forename', $_POST) && array_key_exists('lastname', $_POST) && array_key_exists('address', $_POST) && array_key_exists('postcode', $_POST) && array_key_exists('email', $_POST) && array_key_exists('termsAccepted', $_POST) && array_key_exists('ageVerified', $_POST) ){
	$isValid = true;
	
	$nonBlankFields = array( 'forename', 'lastname', 'address', 'postcode', 'email' );
	$cbFields = array( 'termsAccepted', 'ageVerified' );
	
	foreach( $nonBlankFields as $field ){
		$fields[] = $_POST[$field];
		if( preg_match('/^[\s]*$/' , $_POST[$field] ) ){
			$isValid = false;
			$errors[] = $field;
		}
	}
	
	foreach( $cbFields as $field ){
		$fields[] = $_POST[$field];
		if( $_POST[$field] == 'FALSE' ){
			$isValid = false;
			$errors[] = $field;
		}
	}
	
}else{
	$isValid = false;
	$errors = array( 'forename', 'lastname', 'address', 'postcode', 'email', 'termsAccepted', 'ageVerified' );
}

header('Content-Type: application/json');
// echo json_encode( array( 'status' => ($isValid) ? 'OK' : 'FAIL', 'fields' => $fields, 'errors' => $errors ) );
echo json_encode( array( 'status' => ($isValid) ? 'OK' : 'FAIL', 'errors' => $errors ) );
?>