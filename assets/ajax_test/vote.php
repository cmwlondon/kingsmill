<?php
/* server side validation / AJAX submission / response test file */

$isValid = false;
header('Content-Type: application/json');
if( $_POST && array_key_exists('place', $_POST) && array_key_exists('fbuser', $_POST) && array_key_exists('votes', $_POST) ){
	$isValid = true;

	$place = $_POST['place'];
	$fbuser = $_POST['fbuser'];
	$votes = (int) $_POST['votes'];
	$votes++;
	
	echo json_encode( array( 'status' => 'OK', "place" => $place, "fbuser" => $fbuser, "votes" => $votes) );
}else{
	echo json_encode( array( 'status' => 'FAIL' ) );
}

?>