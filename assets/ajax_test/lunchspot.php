<?php
/* server side validation / AJAX submission / response test file */

$errors	= array();

if( $_POST && array_key_exists('name', $_POST) && array_key_exists('description', $_POST) && array_key_exists('tags', $_POST) ){
	$isValid = true;
	$name = $_POST['name'];
	$description = $_POST['description'];
	$tags = $_POST['tags'];
	$tagCount = count( split(',', $tags) );
	// validate against empty name and less than three tags selected
	
	if( preg_match('/^[\s]*$/' , $name ) ){
		$isValid = false;
		$errors[] = 'name';
	}

	if( preg_match('/^[\s]*$/' , $description ) ){
		$isValid = false;
		$errors[] = 'description';
	}
	
	if($tagCount < 3){
		$isValid = false;
		$errors[] = 'tags';
	}
}else{
	$isValid = false;
	$errors = array( 'name', 'description', 'tags' );
}

header('Content-Type: application/json');
//echo json_encode( array( 'status' => ($isValid) ? 'OK' : 'FAIL', 'fields' => array($name, $tags), 'errors' => $errors ) );
echo json_encode( array( 'status' => ($isValid) ? 'OK' : 'FAIL', 'errors' => $errors ) );
?>