/* ---------------------------------------------------------------------------------- */
// twitterFeed
/* ---------------------------------------------------------------------------------- */

String.prototype.linkify = function() {
	
  return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&;\?\/.=]+/g,function(m){
    return m.link(m);
  });
	
};

String.prototype.linkuser = function() {
	
  return this.replace(/[@]+[A-Za-z0-9-_]+/g,function(u){
    return u.link("http://twitter.com/"+u.replace("@",""));
  });
	
};

String.prototype.linktag = function() {

  return this.replace(/[]+[A-Za-z0-9-_]+/,function(t){
    return t;
  });
  
};

function twitterFeed( parameters ){
	this.account = parameters.account
	this.searchCriteria = parameters.searchCriteria;
	this.rate = parameters.rate;
	this.url = 'assets/helpers/twitter.php';
	this.tweets = [];
	this.feedTimer;
	this.displayTimer;
	this.lastID = 1;
	this.tweetcount = 0;
	
	this.init();
}

twitterFeed.prototype = {
	"constructor" : infoOverlay,

	"init" : function(){
		var _self = this;
		this.getFeed();	

		$('#twitterTarget').empty();
		
		/* user_timeline request rate limited at 150 request every fifteen minutes */
		this.feedTimer = setInterval(function() { _self.getFeed(); }, 300000 ); //  update tweet cache every five minutes
		this.displayTimer = setInterval(function() { _self.displayTweets(); }, 2000 ); // update display every 2 seconds

	},
	"getFeed" : function(){
		var _self = this;
	
		jQuery.getJSON(
			_self.url, 
			{
				"lastid" : _self.lastID,
				"search" : encodeURIComponent( _self.searchCriteria ),
				"account" : _self.account
			},
			function(data) {
				// var results = data.results;
				var results = data;
				if ( results[0] ) {
					var latestTweet = results[0];
					if(results.length >= 1 && _self.lastID != results[0].id){
						// store id of latest tweet
						_self.lastID = results[0].id;

						// change order of tweets to oldest tweet first
						results.reverse();

						for ( var i = 0; i < results.length; i++ ) {
							_self.tweets.push( results[i] );
						}
					}
				}
			}
		);
			
	},
	"displayTweets" : function(){
		var _self = this;
		
		if( this.tweets[0] ) { 
			twip_tweets = this.tweets;
			var thisTweet = twip_tweets[0];
	
			_self.buildTweet( thisTweet );
			
			twip_tweets.splice(0, 1);
			
			if(this.tweetcount >= 4) { 
				$('div#twitterTarget div.twItem:last').remove();
				$('div#twitterTarget div.hr:last').remove();
			} else {
				this.tweetcount ++;
			}
		}
	},
	"buildTweet" : function( tweet ){
		var itemNode = $('<div></div>').addClass('twItem');
		
		var avatarLink = $('<a></a>')
		.attr({
			"href": 'http://www.twitter.com/' + tweet.user.screen_name,
			"target": "_blank"
		});
		var itemImg = $('<img></img>').attr({
			"src" : tweet.user.profile_image_url,
			"alt" : tweet.user.name
		});
		var alignerNode = $('<div></div>').addClass('textAligner');
		var headerNode = $('<h3></h3>').html( '<a href="http://www.twitter.com/' + tweet.user.screen_name + '" target="_blank">'+tweet.user.name + ' @' + tweet.user.screen_name +'</a>' );
		var textNode = $('<p></p>').html( tweet.text.linkify().linkuser().linktag() );

		var controls = $('<ul></ul>').addClass('tweetControls');
		var controlsReply = $('<li></li>').addClass('reply')
		.append( $('<a></a>')
			.attr({
				"href": 'http://twitter.com/home?status=@' + tweet.user.screen_name + '&in_reply_to_status_id=' + tweet.id + '&in_reply_to=' + tweet.user.id,
				"target": "_blank"
			})
			.text('Reply')
		);
		var controlsRetweet = $('<li></li>').addClass('retweet')
		.append( $('<a></a>')
			.attr({
				"href": 'http://twitter.com/home?status=RT%20@' + tweet.user.screen_name + ':%20' + escape( tweet.text ),
				"target": "_blank"
			})
			.text('Retweet')
		);
		var controlsProfile = $('<li></li>').addClass('profile')
		.append( $('<a></a>')
			.attr({
				"href": 'http://www.twitter.com/' + tweet.user.screen_name,
				"target": "_blank"
			})
			.text('Profile')
		);
		controls
		.append(controlsReply)
		.append(controlsRetweet);

		var timeNode = $('<p></p>').html( tweet.created_at );
		
		var hrNode = $('<div></div>')
		.addClass('hr')
		.append( $('<hr>') );
		
		alignerNode
		.append( controls )
		.append( headerNode ) 
		.append( timeNode )
		.append( textNode );

		avatarLink
		.append( itemImg );
		
		itemNode
		.append( avatarLink )
		.append( alignerNode );
		
		$('#twitterTarget')		
		.prepend( itemNode )
		.prepend( hrNode )
	}
}