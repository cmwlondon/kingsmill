/* ---------------------------------------------------------------------------------- */
// mqVideoPlayer
/* ---------------------------------------------------------------------------------- */
/*
popup video player
*/
function mgVideoPlayer(parameters){
	this.overlay = parameters.overlay;
	this.playerID = parameters.playerID;
	this.defaultWidth = parameters.width;
	this.defaultHeight = parameters.height;
	this.width = this.defaultWidth;
	this.height = this.defaultHeight;
	this.leftOffset = Math.floor( this.width / 2 );
	this.topOffset = Math.floor( this.height / 2 );
	this.videoPath = parameters.videoPath;
	this.prefix = parameters.prefix;
	this.playerObject = null;
	this.aspectRatio = this.defaultWidth / this.defaultHeight;
	
	this.install();
}
mgVideoPlayer.prototype.install = function(){
	var _self = this;

	
	$('.playNow').click(function(e){
		e.preventDefault();
		_self.start( this );
	});

	// click on 'CLOSE' link to close overlay
	$(this.overlay).find('a.closer')
	.click( function(e){
		e.preventDefault();
		_self.hidePlayer();
	} );
	
	// press 'ESC' key to close overlay
	$(document)
	.keyup(function(e) {
		if (e.keyCode == 27){
			_self.hidePlayer();
		}
	});
	
	$('.shroud').click(function(e){
			_self.hidePlayer();
	});
	
	/*
	this.resize();
	$(window).resize(function(){
		_self.resize();
	});
	*/
}
mgVideoPlayer.prototype.resize = function(  ){
	var windowWidth = $('html').width();

	if( $('html').width() < 1024 ){
		this.width = Math.floor( $('html').width() * 0.8 );
		this.height = Math.floor(this.width / this.aspectRatio);
	}else{
		this.width = this.defaultWidth;
		this.height = this.defaultHeight;
	}
	this.leftOffset = Math.floor( this.width / 2 );
	this.topOffset = Math.floor( this.height / 2 );
}
mgVideoPlayer.prototype.rebind = function( playLinks ){
	var _self = this;
	playLinks.unbind('click');
	playLinks.click(function(e){
		e.preventDefault();
		_self.start( this );
	});
}

mgVideoPlayer.prototype.start = function( node ){
	var videoFile, videoTitle, playerObject, typePlayer;

	// read video fielname from href 'play/[videoTitle]' 
	// read title from sibling h3 tage
	videoFile = $(node).attr('href');
	var youtubeRegEx = /([a-zA-Z0-9_-]{11})/;
	var results = youtubeRegEx.exec( videoFile );
	var videoID = (results) ? results[1] : 0;

	this.resize();
	$(this.overlay).css({"width" : this.width + "px", "height" : this.height + "px", "margin-left" : -this.leftOffset + "px", "margin-top" : -this.topOffset + "px" });
	
	/*
	insert youtube iframe
<iframe class="youtube-player" type="text/html" width="800" height="480" src="http://www.youtube.com/embed/55OnzNYRIH4?autoplay=1" frameborder="0">
</iframe>	
	*/
	
	var iframe = $('<iframe></iframe>')
	.attr({"width": this.width + "px", "height" : this.height + "px", "type":"text/html", "frameborder":"0", "src":"http://www.youtube.com/embed/" + videoID + "?autoplay=1"})
	.addClass('youtube-player');
	$('#videoPlayer').append(iframe);
	
	this.showPlayer();
}
mgVideoPlayer.prototype.showPlayer = function( ){
	$('.shroud').show();
	$(this.overlay)
	.css({"opacity" : 0, "display" : "block"})
	.animate( {"opacity" : 1}, 250, function(){
	});
	
}
mgVideoPlayer.prototype.hidePlayer = function( ){
	$('#videoPlayer').empty();
	$(this.overlay)
	.animate( {"opacity" : 0}, 250, function(){
		$(this).css({"display" : "none"});
		$('.shroud').hide();
	});
}
