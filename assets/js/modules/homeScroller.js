/* ---------------------------------------------------------------------------------- */
// homeScroller
/* ---------------------------------------------------------------------------------- */
function homeScroller( parameters ){
	this.viewportWidth = 0;
	this.panelWidth = 0;
	this.panelOffset = 0;
	this.windowWidth = 0;
	
	this.videoPlayer = parameters.videoPlayer;
	this.infoOverlay = parameters.infoOverlay;
	
	this.step = 1;
	
	this.install();
}

homeScroller.prototype.install = function(){
	var _self = this;
	
	var tempPanel = $('.panelScroller div.panel').eq(2).clone();
	$('.panelScroller div.panel').eq(2).remove();
	$('.panelScroller').prepend( tempPanel );

	this.update();
	this.updateHeight();
	$(window).resize(function(){
		_self.update();
		_self.updateHeight();
	});
	
	/*
	$('a.homeArrow.left')
	.mouseover(function(){
		$('.panelScroller')
		.animate({"left" : ((_self.offset - _self.viewportWidth) + 50) + "px"}, 500, function(){} );
	})
	.mouseout(function(){
		$('.panelScroller')
		.stop()
		.animate({"left" : (_self.offset - _self.viewportWidth) + "px"}, 500, function(){} );
	})

	$('a.homeArrow.right')
	.mouseover(function(){
		$('.panelScroller')
		.animate({"left" : ((_self.offset - _self.viewportWidth) - 50) + "px"}, 500, function(){} );
	})
	.mouseout(function(){
		$('.panelScroller')
		.stop()
		.animate({"left" : (_self.offset - _self.viewportWidth) + "px"}, 500, function(){} );
	})
	*/
	
	$('a.homeArrow.left').click(function(e){
		e.preventDefault();
		_self.doAction( $(this), $(this).attr('class') );
	});
	$('a.homeArrow.right').click(function(e){
		e.preventDefault();
		_self.doAction( $(this), $(this).attr('class') );
	});
}
homeScroller.prototype.update = function(){
	var _self = this;

	// this.windowWidth = $(document).width();
	this.windowWidth = $('html').width();
	
	// set gutter widths for desktop / mobile context
	this.gutterWidth = (this.windowWidth <= 480) ? 30 : 60;

	this.panelWidth = this.windowWidth;
	if( this.windowWidth >= (1140 + (this.gutterWidth * 2)) ){
		this.offset = ( this.windowWidth - 1140) / 2;
		this.coverWidth = this.offset;
		this.viewportWidth = 1140;
	}else{
		this.offset = this.gutterWidth;
		this.coverWidth = this.gutterWidth;
		this.viewportWidth = this.windowWidth - (this.gutterWidth * 2);
	}
	
	$('.panelWrapper').css({"width" : this.panelWidth + "px", "left" : -this.offset + "px"});
	$('.leftFlyOutCover').css({"width" : this.coverWidth + "px", "left" : -this.offset + "px"});
	$('.rightFlyOutCover').css({"width" : this.coverWidth + "px", "right" : -this.offset + "px"});
	$('.panel').css({"width" : this.viewportWidth + "px"});
	$('.panelScroller').css({"width" : 4 * this.viewportWidth + "px", "left" : (this.offset - this.viewportWidth) + "px" });
	
};
homeScroller.prototype.updateHeight = function(){
	var panelHeight = $('.panel').eq(1).height();
	if( this.windowWidth > 1260 ){
		// desktop context
		var workingHeight = 565
	}else{
		 if( ( this.windowWidth <= 1260 ) && ( this.windowWidth > 768  ) ){
			// calculate panel height
			// var adjust = ((1260 - this.windowWidth) / 492) * 20;
			var adjust = 5;
			var workingWidth = ( this.windowWidth > 1260 ) ? 1260 : this.windowWidth;
			var workingHeight = (565 * ((workingWidth - 120) / 1140)) + adjust;
		}else if( ( this.windowWidth <= 768 ) && ( this.windowWidth > 480  ) ){
			// iPad portrait context
			
			// aspect ratio 1.610 628px * 390px
			var workingHeight = (390 * ((this.windowWidth - 120) / 628));
			
		}else if( ( this.windowWidth <= 480 ) && ( this.windowWidth > 320  ) ){
			// iPhone landscape context
			var workingHeight = 280;
		}else if( this.windowWidth <= 320  ){
			// iPhone portrait context
			var workingHeight = 380;
		}else{	
			var workingHeight = panelHeight;
		}
		$('.panelScroller, .leftFlyOutCover, .rightFlyOutCover, .homeScrollerInner, .panel').css({ "height" : workingHeight + "px"});
		$('.panelInner').css({ "height" : (workingHeight - 20) + "px"});
	}
	
}
homeScroller.prototype.doAction = function( triggerLink, trigger ){
	if( !triggerLink.hasClass('debounce') ){
		var actionRegEx = /(left|right)/;
		var results = actionRegEx.exec( trigger );
		var action = (results) ? results[1] : 0;
		var newOffset = 0;
	
		triggerLink.addClass('debounce');
		
		switch(action){
			case "left" : {
				this.doLeft( triggerLink );
			} break;
			case "right" : {
				this.doRight( triggerLink );
			} break;
		}
	
		$('.homePageContent')
		.removeClass('step1')
		.removeClass('step2')
		.removeClass('step3')
		.addClass('step' + this.step);
	}

}
homeScroller.prototype.doLeft = function( triggerLink ){
	var _self = this;

	this.step--;
	this.loopStep();
	
	// copy last element to start of list
	var tempPanel = $('.panelScroller div.panel').eq(2).clone();
	$('.panelScroller')
	.prepend( tempPanel )
	.css({ "left" : ( -( 2 * this.viewportWidth ) + this.offset ) + "px" });
	
	/* jquery */
	$('.panelScroller').animate({ "left" : (-this.viewportWidth + this.offset ) + "px" }, 500, function(){
		$('.panelScroller div.panel').eq(3).remove();
		_self.endAction( triggerLink );
	});
	
}
homeScroller.prototype.doRight = function( triggerLink ){
	var _self = this;

	this.step++;
	this.loopStep();

	// copy first element to end of list
	var tempPanel = $('.panelScroller div.panel').eq(0).clone();
	$('.panelScroller')
	.append( tempPanel );

	$('.panelScroller').animate({ "left" : ( -(2 * this.viewportWidth) + this.offset ) + "px" }, 500, function(){
		$('.panelScroller div.panel').eq(0).remove();
		$('.panelScroller').css({ "left" : _self.offset - _self.viewportWidth + "px"});
		_self.endAction( triggerLink );
	});

}
homeScroller.prototype.endAction = function( triggerLink ){
	triggerLink.removeClass('debounce');
	var playLinks = $('.panelScroller div.panel').eq(1).find('.playNow');
	if( playLinks.length != 0 ){
		this.videoPlayer.rebind( $('.panelScroller div.panel').eq(1).find('.playNow') );
		this.infoOverlay.bindOpeners('.panel:eq(1)');
	}
	// rebind events
}
homeScroller.prototype.loopStep = function(  ){
	if( this.step > 3 )
		this.step = 1;
	if( this.step < 1 )
		this.step = 3;
}
