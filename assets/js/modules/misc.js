/* http://joncom.be/code/javascript-rounding-errors/ */
function Round(Number, DecimalPlaces) {
   return Math.round(parseFloat(Number) * Math.pow(10, DecimalPlaces)) / Math.pow(10, DecimalPlaces);
}
function RoundFixed(Number, DecimalPlaces) {
   return Round(Number, DecimalPlaces).toFixed(DecimalPlaces);
}

/* ---------------------------------------------------------------------------------- */
// externalLink
/* ---------------------------------------------------------------------------------- */

function externalLink(container){
	this.container = container;
	this.install();
}
externalLink.prototype.template = function(){};
externalLink.prototype.install = function(){
	$(this.container + ' .external').click(function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		window.open( url,'_blank' );
	});
};

/* ---------------------------------------------------------------------------------- */
// timestamp
/* ---------------------------------------------------------------------------------- */

function timestamp(){
	this.year;
	this.month;
	this.day;
	this.date;
	this.hours;
	this.minutes;
	this.seconds;
	this.time;
	this.timezone;
	
	this.now();
}
timestamp.prototype.template = function(){};
timestamp.prototype.now = function(){
	var today = new Date();
	this.year = today.getFullYear();
	this.month = today.getMonth()+1;
	this.date = today.getDate();
	this.day = today.getDay();
	this.hours = today.getHours();
	this.minutes = today.getMinutes();
	this.seconds = today.getSeconds();
};
timestamp.prototype.setDate = function( dateObject ){ this.year = dateObject.year; this.month = dateObject.month; this.date = dateObject.date;  };
timestamp.prototype.setTime = function( timeObject ){ this.hours = timeObject.hours; this.minutes = timeObject.minutes; this.seconds = timeObject.seconds;  };
timestamp.prototype.zeroPad = function( number ){ return ( number <= 9 ) ? '0' + number : number; };

timestamp.prototype.returnTimestamp = function(){ return '' + this.year + this.zeroPad(this.month) + this.zeroPad(this.date) + this.zeroPad(this.hours) + this.zeroPad(this.minutes) + this.zeroPad(this.seconds)};

/* ---------------------------------------------------------------------------------- */
// timerObject
/* ---------------------------------------------------------------------------------- */

function timerObject(parameters){
	this.timer;
	this.delay;
	
	if(typeof parameters != 'undefined'){
		if(typeof parameters.delay != 'undefined'){
			this.setDelay(parameters.delay);
		}
		if(typeof parameters.process != 'undefined'){
			this.setProcess(parameters.process);
		}
	}
}
timerObject.prototype.template = function( parameters ){ return; }; 
timerObject.prototype.startTimer = function(){
	this.timer = window.setInterval(this.process,this.delay);
}; 
timerObject.prototype.killTimer = function(){
	clearInterval(this.timer);
}; 

timerObject.prototype.setDelay = function(delay){
	this.delay = delay;
}

timerObject.prototype.setProcess = function(process){
	this.process = process;
}; 

/* ---------------------------------------------------------------------------------- */
// isNotBlank
/* ---------------------------------------------------------------------------------- */

function isNotBlank( fieldValue ){
	var blankRegEx = /^[\s]*$/g;
	return !blankRegEx.exec( fieldValue );
}

/* ---------------------------------------------------------------------------------- */
// countWords
/* ---------------------------------------------------------------------------------- */

function countWords( string ){
	string = string.replace(/(^\s*)|(\s*$)/gi,"");
	string = string.replace(/[ ]{2,}/gi," ");
	string = string.replace(/\n /,"\n");
	return string.split(' ').length;
}	

