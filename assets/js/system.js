var pagetTimestamp = new timestamp();
pagetTimestamp.now();
var isIE6,isIE67,isIE8,isIE9;

// user agent sniffing for iOS devices
var ua = navigator.userAgent;
var iOSDevice = (ua.match(/iPad|iPod|iPhone/));
var androidDevice = (ua.match(/Android/));

var enableMarkerPlacement = true;
var newPin = false;
var newPlace = '';
var newMarker = false;
var map;
var ib;
var thisPlace;

var emptyRegEx = new RegExp(/^(\s)*$/);
var searchDefault = 'Enter postcode/place name';
var descriptionDefault = '100 words max';
var facebookID = 8817;
var newFile = '';
var isFileSelected = false;
var isFileUploaded = false;
var currentPlace = false;
var currentMarker = false;
var currentMapState = 0; // default view: map
var directionsService = false;
	
function ellipsis( string, wordcount ){
	var words = words2 = string.split(' ');
	var trail = '';
	var clipped = false;
	if( words.length > wordcount ){
		words2 = words.splice(0, wordcount);
		trail = '&hellip;';
		clipped = true;
	}
	var output = words2.join(' ');
	return { "text" : output + trail, "clipped" : clipped };
	
	/*
	var ellipsisRegEx = new RegExp( "(\\s*(?:[^\\s]+[\\,\\.\\;\\:\\;\\?\\!]*)){1," + words + "}", "" );
	var matches = ellipsisRegEx.exec( string );
	if( matches != null ){
		return matches[0] + "&hellip;";
	}
	return false;
	*/
}

$(document).ready(function(){
	isIE6 = $('html').hasClass('ie6');
	isIE7 = $('html').hasClass('ie7');
	isIE8 = $('html').hasClass('ie8');
	isIE9 = $('html').hasClass('ie9');

/*
----------------------------------------------------------------------------------------------------------------
general google map properties/settings
----------------------------------------------------------------------------------------------------------------
*/
	/* set default center, zoom level and marker placement bounds for UK */	
	var ukCentre = new google.maps.LatLng(54.572062,-4.96582);
	var ukZoom = 5;
	/* approximate bounding coordinates for UK */
	var ukNE = new google.maps.LatLng( 58.832, 1.914 );
	var ukSW = new google.maps.LatLng( 49.954, -11.055 );
	var boundsUK = new google.maps.LatLngBounds( ukSW, ukNE );
	var geocoder = new google.maps.Geocoder();
	
	// create custom markerImage using our png icon
	var klsPin = new google.maps.MarkerImage(
		'assets/img/marker/map1.png',
		new google.maps.Size(20, 34),
		new google.maps.Point(0,0),
		new google.maps.Point(10, 34)
	);		

	// create custom markerImage for lunch spot info overlay
	var kls2Pin = new google.maps.MarkerImage(
		'assets/img/marker/map2.png',
		new google.maps.Size(79,55),
		new google.maps.Point(0,0),
		new google.maps.Point(15,49)
	);		

	var kls3Pin = new google.maps.MarkerImage(
		'assets/img/overlay/step2pin.png',
		new google.maps.Size(163,103),
		new google.maps.Point(0,0),
		new google.maps.Point(32,99)
	);		

	var defaultOptions = {
		"center" : ukCentre,
		"zoom" : ukZoom,
		"mapTypeId": google.maps.MapTypeId.ROADMAP,
		"panControl" : true,
		"panControlOptions" : {
			"position" : google.maps.ControlPosition.LEFT_BOTTOM
		},
		"zoomControl" : true,
		"zoomControlOptions" : {
			// "style" : google.maps.ZoomControlStyle.DEFAULT,
			"style" : google.maps.ZoomControlStyle.SMALL,
			"position" : google.maps.ControlPosition.LEFT_BOTTOM
		},
		"mapTypeControl" : false // conceal map type selector
	};

	var placePinOptions = defaultOptions;
	placePinOptions.disableDoubleClickZoom = true;

	var insetMapOptions = {
		// "center" : placeMarker,
		"zoom" : 13,
		"panControl" : false,
		"zoomControl" : true,
		"zoomControlOptions" : {
			// "style" : google.maps.ZoomControlStyle.DEFAULT,
			"style" : google.maps.ZoomControlStyle.SMALL,
			"position" : google.maps.ControlPosition.LEFT_BOTTOM
		},
		"mapTypeControl" : false, // conceal map type selector
		"mapTypeId": google.maps.MapTypeId.ROADMAP
	};
	
/*
----------------------------------------------------------------------------------------------------------------
javascript 'placeholder' for text fields - provides same functionality as HTML 5 placeholder property
for browsers which don't support placeholder
----------------------------------------------------------------------------------------------------------------
*/
	
	if(!Modernizr.input.placeholder){
		$('#gSearch')
		.val( searchDefault )
		.focus(function(e){
			if( $(this).val() == searchDefault ){
				$(this).val('');
			}
		})
		.blur(function(e){
			if( emptyRegEx.exec( $(this).val() ) || $(this).val() == searchDefault ){
				$(this).val(searchDefault);
			}
		});
	}
	
	if(!Modernizr.input.placeholder){
		$('#description')
		.val( descriptionDefault )
		.focus(function(e){
			if( $(this).val() == descriptionDefault ){
				$(this).val('');
			}
		})
		.blur(function(e){
			if( emptyRegEx.exec( $(this).val() ) || $(this).val() == descriptionDefault ){
				$(this).val( descriptionDefault );
			}
		});
	}

/*
----------------------------------------------------------------------------------------------------------------
generic overlay function
----------------------------------------------------------------------------------------------------------------
*/
	function openOverlay( parameters ){
		// close overlay if user clicks on close button or shroud
		$(parameters.overlayID + ' .close, .shroud').click(function(e){
			e.preventDefault();
			
			$('.shroud, ' + parameters.overlayID)
			.stop()
			.animate({"opacity" : 0}, 500, function(){
				$('.shroud, ' + parameters.overlayID)
				.css({ "display" : "none" });

				if( parameters.closePlayload ){
					parameters.closePlayload();
				}
			});
		});	

		// begin process of displaying overlay
		$('.shroud, ' + parameters.overlayID)
		.css({ "display" : "block" });
		
		// run payload before overlay fades in
		// do this to set up google map instance BEFORE the overlay fades, as opposed to displaying a blank overlay 
		if( parameters.payload ){
			parameters.payload();
		}
		
		$('.shroud')
		.stop()
		.animate({"opacity" : 1}, 500, function(){
		});
		$(parameters.overlayID)
		.stop()
		.animate({"opacity" : 1}, 500, function(){
			// run payload after overlay fades in
			// parameters.payload();
		});
	}
	
/*
----------------------------------------------------------------------------------------------------------------
open prizes overlay
----------------------------------------------------------------------------------------------------------------
*/
	$('#likegate .dailyprizes,#home .viewPrizes a,.step3 a.viewPrizes').click(function(e){
		e.preventDefault();
		openOverlay({
			"overlayID" : "#prizeOverlay",
			"payload" : function(){
			}
		});
	});

/*
----------------------------------------------------------------------------------------------------------------
handle 'like us' button on likegate page (index.html)
----------------------------------------------------------------------------------------------------------------
*/
	$('#likegate .likeus').click(function(e){
		e.preventDefault();
		alert('trigger FB like');
		/*
		add code to trigger facebook like, directs user to home.html
		*/
	});

	/*
	$('#home .enter a').click(function(e){
		e.preventDefault();
	});
	*/

/*
----------------------------------------------------------------------------------------------------------------
open view/vote overlay when user clicks on a 'view' button in the top 25 list page (top25_list.html)
----------------------------------------------------------------------------------------------------------------
*/
	// $('#top25b a.viewPlace').click(function(e){
	$('#top25b .shortList li').click(function(e){
		e.preventDefault();
		_self = this;
		openOverlay({
			"overlayID" : "#lunchSpotsInfoOverlay",
			"payload" : function(  ){
				// var placeIndex = $('.shortList li').index( $(_self).parents('li').eq(0) );
				var placeIndex = $('.shortList li').index( $(_self) );
				thisPlace = places[ placeIndex ];
				$('.dynamicPinTitle').text( thisPlace.title );
				$('.description').text( thisPlace.description );
				$('.currentVotes span').text( thisPlace.votes );
				var voteState = ( alreadyVoted )? "none" : "block";				
				$('.voteBlock').css({
					"display" : voteState
				});
				$('.imagery .upload img').attr({
					"alt" : thisPlace.title,
					"src" : thisPlace.images[1]
				})
				
				insetMap( thisPlace );
			}
		});
	});

/*
----------------------------------------------------------------------------------------------------------------
open view overlay when user clicks on a 'view' button in the top 10 list page (top10_list.html)
----------------------------------------------------------------------------------------------------------------
*/
	// $('#top10b a.viewPlace').click(function(e){
	$('#top10b .shortList li').click(function(e){
		
		e.preventDefault();
		_self = this;
		openOverlay({
			"overlayID" : "#toptenSpotsInfoOverlay",
			"payload" : function(  ){
				// var placeIndex = $('.shortList li').index( $(_self).parents('li').eq(0) );
				var placeIndex = $('.shortList li').index( $(_self) );
				thisPlace = places[ placeIndex ];
				// populate content based on selected place
				$('.dynamicPinTitle').text( thisPlace.title );
				$('.dynamicPinSubTitle').text( thisPlace.placename );
				$('.description').text( thisPlace.description );

				$('.endPlace').text( thisPlace.title ); // set destination name in direction finder widget

				var gallery = thisPlace.images[1];
				var galleryContainer = $('.lunchSpotGallery');
				galleryImages = gallery.split(',');
				for( imageIndex = 0; imageIndex < galleryImages.length; imageIndex++ ){
					var thisImage = galleryImages[ imageIndex ];
					var id = imageIndex + 1;
					var newGalleryItem = $('<li></li>').attr({"id" : "img" + id });
					var newGalleryImage = $('<img></img>').attr({"alt" : thisPlace.title + " " + id, "src" : "assets/img/top10/" + thisImage });
					newGalleryItem.append( newGalleryImage );
					galleryContainer.append( newGalleryItem );
				}

				// set up google directions service
				directionsService = new google.maps.DirectionsService();
				
				// make sure map widget is showing and set directions widget to default state
				currentMapState = 0;
				$('.mapSwitch')
				.removeClass('directionsState')
				.addClass('mapState');
				$('.directionsWidget').hide();
				$('.mapWidget').show();
				$('#steps').empty();
				$('#start').val('')
				$('#directionsListWrapper').css({ "display" : "none", "opacity" : 0 });
				$('#directionsForm').css({"opacity" : 1, "display" : "block" });

				// add minimap to overlay, centred on current place, with large marker pin
				insetMap( thisPlace );

				
				currentPlace = thisPlace;
				$('#toptenSpotsInfoOverlay .share').click(function( e ){
					e.preventDefault();
					sharePlace( currentPlace );
				});
			},
			"closePlayload" : function(){
				currentPlace = false;
			}
		});
	});
	
/*
----------------------------------------------------------------------------------------------------------------
handle 'vote' button in info/vote overlay in top 25 list (top25_list.html)
----------------------------------------------------------------------------------------------------------------
*/
	$('#lunchSpotsInfoOverlay .voteLink').click(function(e){
		e.preventDefault();
		
		var placeIndex = $('.shortList li').index( $( _self ) ); // _self relates to the grid item which opened the overlay
		thisPlace = places[ placeIndex ];
		
		// make AJAX call to register 1 new vote for place thisPlace.id by facebook user thisPlace.fb
		$.ajax({
			"type" : "POST",
			"url" : "assets/ajax_test/vote.php",
			"data" : {
				"place" : thisPlace.id,
				"fbuser" : thisPlace.fb,
				"votes" : thisPlace.votes
			},
			"dataType" : "text",
			"cache" : false,
			"success" : function( text ){
				response = $.parseJSON( text );
			},
			"complete" : function(){
				switch( response.status ){
					case 'OK' : {
						$('#lunchSpotsInfoOverlay .currentVotes span').text( response.votes );
						$('.shortList li').eq(placeIndex).find('p.votes span').text( response.votes );
						places[ placeIndex ].votes = response.votes;
						
						//hide vote button in overlay once the use has voted on a place
						alreadyVoted = true;
						$('#lunchSpotsInfoOverlay .voteBlock').fadeOut( 500, function(){
							$(this).hide();
						});
						
						// vote registered ok, JSON response includes
						// place id
						// faceboook user id
						// new number of votes
					} break;
					case 'FAIL' : {
					} break;
				}
			}
		});
	});
	
/*
----------------------------------------------------------------------------------------------------------------
open 'View lunch spots' google map popup 
----------------------------------------------------------------------------------------------------------------
*/
	$('#home .viewLunchSpots a, a.viewLunchSpots').click(function(e){
		e.preventDefault();
		openOverlay({
			"overlayID" : "#viewLunchSpotsOverlay",
			"payload" : function(){
				// create map instance in page in div#googleMap
				map = new google.maps.Map( document.getElementById("googlePopupMap"), defaultOptions );
		
				// place markers on map based on contents of JSON object via AJAX call
				// ajaxMarkers();
				
				// place markers on map based on contents of JSON object in page: places
				jsonMarkers();
			}
		});
	});

/*
----------------------------------------------------------------------------------------------------------------
handle place/postcode search in initial state of map page (map.html)
----------------------------------------------------------------------------------------------------------------
*/
	$('#enterLunchSpot #gSearchGo').click(function(e){
		e.preventDefault();

		var location = $('#gSearch').val(); 
		/*
		check for:
		blank search field
		default text *ONLY IF* the placeholder property is not supported
		*/
		if( isNotBlank( location ) && !( !Modernizr.input.placeholder && location == 'Enter you favourite lunch spot' ) ){
			
			/*
			https://developers.google.com/maps/documentation/geocoding/
			http://stackoverflow.com/questions/3998654/creating-a-search-form-for-generating-a-google-map
			http://stackoverflow.com/questions/2647086/googles-geocoder-returns-wrong-country-ignoring-the-region-hint
			*/
			
			geocoder.geocode({
				'address': location,
				'region':  'uk',
				'bounds':  boundsUK
			}, 
			/*
			geocoder.geocode({
				"address" : location + ',uk' // append 'uk' to search term, simplest way to limit location searches to uk region
			}, 
			*/
			function(results, status) {
				if(status == google.maps.GeocoderStatus.OK) {
					var place = results[0].geometry.location;
					$('.mapFurniture,#googleMap,.mapFurniture .left,.mapFurniture .right').animate({"height" : "550px" }, 500, function(){
						$('.conditional').slideUp();
						google.maps.event.trigger(map, "resize");
						//map.panTo( place );
						map.setCenter(place);
						map.setZoom(12);
					});

				}
			});
			
		}
	});

/*
----------------------------------------------------------------------------------------------------------------
open terms and conditions (prize draw entry form - phase 3)
----------------------------------------------------------------------------------------------------------------
*/
	$('a.termsTriggerPopup').click(function(e){
		e.preventDefault();
		openOverlay({
			"overlayID" : "#termsOverlay",
			"payload" : function(){
			}
		});
	});

/*
----------------------------------------------------------------------------------------------------------------
set up google map in map page (map.html)
add event handling to allow users to drop a pin to nominate lunch spots in UK
----------------------------------------------------------------------------------------------------------------
*/
	if( $('#googleMap').length != 0 ){
	
		// create map instance in page in div#googleMap
		map = new google.maps.Map( document.getElementById("googleMap"), placePinOptions );

		// add new lunch spot
		google.maps.event.addListener(map, 'click', function(e) { // triggers in SINGLE CLICK at the moment for testing purposes
		
		/* switch to double click for final version */
		// google.maps.event.addListener(map, 'dblclick', function(e) {

			if( enableMarkerPlacement ){
				// prevent marker placement until the user reaches phase 5
				var startLocation = e.latLng;
				
				// check to see if marker lies within lat/lng bounds for UK
				if( boundsUK.contains( startLocation ) ){
					// marker is within bounds

					// use google reverse geocode to convert lat/lng to place name/postcode
					// https://developers.google.com/maps/documentation/geocoding/#ReverseGeocoding
					geocoder.geocode({
						"latLng" : startLocation
					}, 
					function(results, status) {
						switch(status){
							case google.maps.GeocoderStatus.OK : {
								newPlace = results[0].formatted_address;

								var acIndex = 0
								var thisAC = results[0].address_components[ acIndex ]
								while( thisAC.types[0] != 'country' ){
									thisAC = results[0].address_components[ acIndex ]
									acIndex++;
								}

								/*
								UK -> 'United Kingdom' / 'GB'
								Isle of Man -> 'Isle of Man'
								*/
								if( thisAC.long_name == 'United Kingdom' ){
									// map.panTo( startLocation );
									newPin = {"lat" : startLocation.lat(), "lng" : startLocation.lng(), "place" : newPlace };
									phase1();
									
								}
							} break;
							case google.maps.GeocoderStatus.ZERO_RESULTS : 
							case google.maps.GeocoderStatus.OVER_QUERY_LIMIT : 
							case google.maps.GeocoderStatus.REQUEST_DENIED : 
							case google.maps.GeocoderStatus.INVALID_REQUEST : {
								newPlace =  "address not found";
							} break;
						}
					});

				}else{
					// handle marker outside of UK
				}
			}
		});

		// place markers on map based on contents of JSON object in page: places
		jsonMarkers();
	}
	
/*
----------------------------------------------------------------------------------------------------------------
set up jScrollPane instance in top 25 and top 10 grid pages
----------------------------------------------------------------------------------------------------------------
*/
	if( $('.shortlistWrapper').length != 0 ){
		$('.shortlistWrapper').jScrollPane(
			{
				"verticalDragMinHeight" : 25,
				"verticalDragMaxHeight" : 25
			}
		);
	}

/*
----------------------------------------------------------------------------------------------------------------
build popup lunch spots map based on AJAX JSON call
----------------------------------------------------------------------------------------------------------------
*/
	function ajaxMarkers(){
		var places;
		var markerList = new Array();
		$.ajax({
			"type" : "GET",
			"url" : "assets/json/places.js",
			"data" : "",
			"dataType" : "text",
			"cache" : false,
			"success" : function( text ){
				lunchspots = $.parseJSON( text );
			},
			"complete" : function(){
				var marker, thisPlace, thisPlaceTitle, thisPlaceLocation, thisPlaceZindex
				// iterates through places, add ing map marker for each one
				for( placeIndex = 0; placeIndex < lunchspots.places.length; placeIndex++){
					thisPlace = lunchspots.places[ placeIndex ];
					thisPlaceTitle = thisPlace[0];
					thisPlaceLocation = new google.maps.LatLng( thisPlace[1], thisPlace[2] );
					thisPlaceZindex = thisPlace[3];

					markerList[ placeIndex ] = new google.maps.Marker({
						"position" : thisPlaceLocation,
						"map" : map,
						"title" : thisPlaceTitle,
						"icon" : klsPin,
						"zIndex" : thisPlaceZindex
					});

					// markerList.push( marker );
					google.maps.event.addListener( markerList[ placeIndex ], 'click', function() {
						var currentZoom = map.getZoom();
						if( currentZoom < 8 ){
							map.setZoom( 8 );
						}
						map.panTo( this.getPosition() );
					});
				}
			}
		});
	}
	
/*
----------------------------------------------------------------------------------------------------------------
build popup lunch spots map based on in-page JSON data
----------------------------------------------------------------------------------------------------------------
*/
	function jsonMarkers(){
		var placeIndex, thisPlace;
		var markerList = new Array();
		for( placeIndex = 0; placeIndex < places.length; placeIndex++){
			thisPlace = places[ placeIndex ];

			markerList[ placeIndex ] = new google.maps.Marker({
				"position" : new google.maps.LatLng( thisPlace.lat, thisPlace.lng ),
				"map" : map,
				"title" : thisPlace.title,
				"icon" : klsPin,
				"description" : thisPlace.description,
				"placename" : thisPlace.placename,
				"image" : ( thisPlace.images[ 0 ] != '' ) ? thisPlace.images[ 0 ] : "assets/img/infobox/logo.gif"
			});

			google.maps.event.addListener( markerList[ placeIndex ], "click", function(e) {
				map.panTo( this.getPosition() );
				
				currentMarker = this; // store current marker so it can be accesses when the infoBox is closed
				this.setIcon( kls2Pin ); // change for larger icon when infoBox is opened
				var shortText = ellipsis( this.description, 15 );
				var moreLink = ( shortText.clipped ) ? "<a href=\"#\" class=\"infoBoxMore\">More &gt;</a>" : "";
				
				// console.log("title: '%s' description: '%s' lat,lng: %s,%s image: '%s'", this.title, shortText.text, this.getPosition().lat(), this.getPosition().lng(), this.image[0] );
				
				ib.setOptions({
					"content" : "<article class=\"body\"><img alt=\"\" src=\"" + this.image + "\"><header><h1>" + this.title + "</h1><h2>" + this.placename + "</h2></header><div class=\"scrollFrame short\"><p class=\"summary\">" + shortText.text + "</p>" + moreLink + "<div class=\"longText\"><p>" + this.description + "</p></div></div></article>"
				});
				ib.open( map, this ); 
			});
		}


	}
/*
----------------------------------------------------------------------------------------------------------------
add inset google map to view/vote overlays in top25/top10 pages
----------------------------------------------------------------------------------------------------------------
*/
	function insetMap( thisPlace ){
		var placeMarker = new google.maps.LatLng( thisPlace.lat, thisPlace.lng );
		insetMapOptions.center = placeMarker;
		map = new google.maps.Map( document.getElementById("googleInsetMap"), insetMapOptions );

		marker = new google.maps.Marker({
			"position" : placeMarker,
			"map" : map,
			"title" : thisPlace.title,
			"icon" : kls2Pin
		});
	}
	
/*
----------------------------------------------------------------------------------------------------------------
map page - adding new lunch spots and entering prize draw
----------------------------------------------------------------------------------------------------------------
functions to handle each step of the process of adding a new lunch spot
phase 0 - default - reverts to this if the user selects 'cancel' in phase 1
phase 1 cancel/confirm
phase 2 set lunch spot title, description and tags, optiomal image upload
phase 3 - prize draw entry form
phase 4 - thanks page
phase 5 - revert back to default map state
*/
	
	// phase 1 'cancel' button
	$('.step1 .cancel').click(function(e){
		e.preventDefault();
		phase0();
	});

	// phase 1 'confirm' button
	$('.step1 .confirm').click(function(e){
		e.preventDefault();
		phase2();
	});

	// phase 2 'submit' button
	$('.step2 .submit').click(function(e){
		e.preventDefault();

		$('.step2 .error').removeClass('error');

		// validate form fields
		// non-blank title
		// at least three tags selected
		var valid = true;
		var errors = new Array();
		var response;
		var title = $('#name').val();
		var description = $('#description').val();
		var image = 'FILE';
		var tagCheckboxes = $('input[name=tags]');
		var tagCount = 0;
		var tagList = '';
		
		for(var tagIndex = 0; tagIndex < tagCheckboxes.length; tagIndex++ ){
			var thisTagCheckbox = $( tagCheckboxes.get(tagIndex) );
			if( thisTagCheckbox.attr('checked') ){
				tagCount++;
				tagList += ( tagList != '' ) ? ',' : '';
				tagList += thisTagCheckbox.attr('value');
			}
		}
		
		if( isFileSelected && !isFileUploaded ){
			valid = false;
			errors.push('file');
		}

		if( !isNotBlank( title ) ){
			valid = false;
			errors.push('name');
		}
		
		if( !isNotBlank( description ) ){
			valid = false;
			errors.push('description');
		}else{
			// non-empty field - validate against 100 word limit
			if( countWords( description ) > 100 ){
				valid = false;
				errors.push('description');
			}
		}
		
		if( tagCount < 3 ){
			valid = false;
			errors.push('tags');
		}

		if(valid){
			// form passes client-side validation
			
			// 1) submit to server via AJAX
			// 2) server validates fields 
			// 3) server responds with JSON message containing either:
			// OK - go on to phase3 : prize draw entry form
			// FAIL - server returns list of invalid fields - highlight invalid fields so user can correct issues
			$.ajax({
				"type" : "POST",
				"url" : "assets/ajax_test/lunchspot.php",
				"data" : {
					"name" : title,
					"description" : description,
					"image" : newFile,
					"tags" : tagList,
					"facebook" : facebookID,
					"lat" : newPin.lat,
					"lng" : newPin.lng,
					"place" : newPin.place
				},
				"dataType" : "text",
				"cache" : false,
				"success" : function( text ){
					response = $.parseJSON( text );
				},
				"complete" : function(){
					switch( response.status ){
						case 'OK' : {
							phase3();
						} break;
						case 'FAIL' : {
							for(var errorIndex = 0; errorIndex < response.errors.length; errorIndex++ ){
								var thisError = response.errors[ errorIndex ];
								
								switch( thisError ){
									case 'name' : {
										$('#step2name').addClass('error');
									} break;
									case 'description' : {
										$('#step2description').addClass('error');
									} break;
									case 'file' : {
										$('.uploadWidget').addClass('error');
									} break;
									case 'tags' : {
										$('#tagLegend').addClass('error');
									} break;
								}
							}
						} break;
					}
				}
			});
		}else{
			// handle errors - highlight invalid fields/labels
			for(var errorIndex = 0; errorIndex < errors.length; errorIndex++ ){
				var thisError = errors[ errorIndex ];
				
				switch( thisError ){
					case 'name' : {
						$('#step2name').addClass('error');
					} break;
					case 'description' : {
						$('#step2description').addClass('error');
					} break;
					case 'file' : {
						$('.uploadWidget').addClass('error');
					} break;
					case 'tags' : {
						$('#tagLegend').addClass('error');
					} break;
				}
			}
		}
	});

	// phase 3 'enter' button
	$('.step3 .enter').click(function(e){
		e.preventDefault();

		$('.step3 .field').removeClass('error');
		$('.step3 .errorPanel').addClass('valid');
		/* field ids:
		#forename nonblank [A-Za-z]
		#lastname nonblank [A-Za-z]
		#address nonblank [A-Za-z0-9\s\,]
		#postcode nonblank, valid postcode
		#email nonblank, valid email address
		cb: #termsAccept
		cb: #ageVerify
		*/
		var valid = true;
		var errors = new Array();
		var response;

		var forename = $('#forename').val();
		var lastname = $('#lastname').val();
		var address = $('#address').val();
		var postcode = $('#postcode').val();
		var email = $('#email').val();
		var promocode = $('#promocode').val();
		var termsAccepted = ( $('#termsAccepted').attr('checked') ) ? 'TRUE' : 'FALSE';
		var ageVerified = ( $('#ageVerified').attr('checked') ) ? 'TRUE' : 'FALSE';
		var newsletterOptin = ( $('#optin').attr('checked') ) ? 'TRUE' : 'FALSE';
		
		if( !isNotBlank( forename ) ){
			valid = false;
			errors.push('forename');
		}
		if( !isNotBlank( lastname ) ){
			valid = false;
			errors.push('lastname');
		}
		if( !isNotBlank( address ) ){
			valid = false;
			errors.push('address');
		}
		if( !isNotBlank( postcode ) ){
			valid = false;
			errors.push('postcode');
		}
		if( !isNotBlank( email ) ){
			valid = false;
			errors.push('email');
		}
		if( termsAccepted == 'FALSE' ){
			valid = false;
			errors.push('termsAccepted');
		}
		if( ageVerified == 'FALSE' ){
			valid = false;
			errors.push('ageVerified');
		}

		if(valid){
			// passes client-side validation, hand over to server
			$.ajax({
				"type" : "POST",
				"url" : "assets/ajax_test/prizedraw.php",
				"data" : {
					"forename" : forename,
					"lastname" : lastname,
					"address" : address,
					"postcode" : postcode,
					"email" : email,
					"termsAccepted" : termsAccepted,
					"ageVerified" : ageVerified,
					"promocode" : promocode,
					"newsletterOptin" : newsletterOptin
				},
				"dataType" : "text",
				"cache" : false,
				"success" : function( text ){
					response = $.parseJSON( text );
				},
				"complete" : function(){
					switch( response.status ){
						case 'OK' : {
							phase4();
						} break;
						case 'FAIL' : {
							$('.step3 .errorPanel').removeClass('valid');
							for(var errorIndex = 0; errorIndex < response.errors.length; errorIndex++ ){
								var thisError = response.errors[ errorIndex ];
								$('#' + thisError).parent().addClass('error');
							}
						} break;
					}
				}
			});
		}else{
			$('.step3 .errorPanel').removeClass('valid');
			for(var errorIndex = 0; errorIndex < errors.length; errorIndex++ ){
				var thisError = errors[ errorIndex ];
				$('#' + thisError).parent().addClass('error');
			}
		}
	});

	// phase 4 'share' button
	$('.step4 .share').click(function(e){
		e.preventDefault();
		// phase5();
		
		// handles creating new facebook postpromoting lunch spots in general and/or the new lunch spot in particular
	});
	
	function phase0(){
		/* remove marker */
		newMarker.setMap( null );
		enableMarkerPlacement = true;

		$('.step1').hide();
		$('#enterLunchSpot .always')
		.removeClass('phase1')
		.addClass('phase0');
		$('.conditional').show();
		$('.mapFurniture,#googleMap,.mapFurniture .left,.mapFurniture .right').css({"height" : "400px" });
		google.maps.event.trigger(map, "resize");
		map.setCenter( ukCentre );
		map.setZoom( ukZoom );

		/* reset map to uk centre and zoom 5 */
		/*
		map.panTo( ukCentre );
		map.setZoom( ukZoom );
		*/

	}
	
	// initial confirm/cancel dialog
	// short header
	function phase1(){
		enableMarkerPlacement = false;
		newMarker = new google.maps.Marker({
			"position" :  new google.maps.LatLng( newPin.lat, newPin.lng ),
			"map" : map,
			"title" : newPin.place,
			"icon" : kls3Pin,
			"animation" : google.maps.Animation.DROP
		});
		
		$('.conditional').hide();
		$('#enterLunchSpot .always')
		.removeClass('phase0')
		.addClass('phase1');
		$('.mapFurniture,#googleMap,.mapFurniture .left,.mapFurniture .right').css({"height" : "580px" });
		/*
		$('.mapFurniture,#googleMap,.mapFurniture .left,.mapFurniture .right').animate({"height" : "580px" }, 500, function(){
			google.maps.event.trigger(map, "resize");
			// map.panTo( place );
		});
		*/
		google.maps.event.trigger(map, "resize");
		map.setCenter( new google.maps.LatLng( newPin.lat, newPin.lng ) );
		var phase0Delay = window.setTimeout(function(){ $('.step1').show(); }, 600);
	}

	// my favourite lunch spot form
	// short header
	function phase2(){
		$('.step1').hide();
		$('.step2').show();
		$('#enterLunchSpot .always')
		.removeClass('phase1')
		.addClass('phase2');
		
		uploader.init();
		
		// remove error markers
		$('.step2 .error').removeClass('error');
		
		// reset form fields/labels to default state;
		$('#name').val('');
		$('#description').val('');
		var tagCheckboxes = $('input[name=tags]');
		for(var tagIndex = 0; tagIndex < tagCheckboxes.length; tagIndex++ ){
			var thisTagCheckbox = $( tagCheckboxes.get(tagIndex) );
			thisTagCheckbox.attr({'checked' : false});
		}
	}
	
	// prize draw entry form
	// fill in form header
	function phase3(){
		$('.step2').hide();
		$('.step3').show();
		$('#enterLunchSpot .always')
		.removeClass('phase2')
		.addClass('phase3');
		
		// reset form fields/labels to default state;
		$('.step3 .field').removeClass('error');
		$('.step3 .errorPanel').addClass('valid');
		
		$('#forename').val('');
		$('#lastname').val('');
		$('#address').val('');
		$('#postcode').val('');
		$('#email').val('');
		$('#promocode').val('');
		$('#termsAccepted').attr({'checked' : false });
		$('#ageVerified').attr({'checked' : false });
		$('#optin').attr({'checked' : false });
	}
	
	// thanks panel
	// thanks header
	function phase4(){
		$('.step3').hide();
		$('.step4').show();
		$('#enterLunchSpot .always')
		.removeClass('phase3')
		.addClass('phase4');
		
		// add link backt on home.html on kingsmill lunch spots logo
		$('.always .logo')
		.unbind('click')
		.css({"cursor" : "pointer"})
		.click(function(e){
			window.location.href = "home.html";
		});
		
		/*
		what does the share button do? facebook / twitter submission?
		Does the user have a choice regarding whether to share or not?
		*/
	}

	function phase5(){
		$('.step4').hide();
		$('#enterLunchSpot .always')
		.removeClass('phase4')
		.addClass('phase1');

		// enable marker placement 
		enableMarkerPlacement = true;
		
		/* reset map to uk centre and zoom 5 */
		map.panTo( ukCentre );
		map.setZoom( ukZoom );
	}

/*
----------------------------------------------------------------------------------------------------------------
AJAX file upload for phase 2 - uses plupload (http://www.plupload.com)
----------------------------------------------------------------------------------------------------------------
*/
	// AJAX file upload handling - for map.html
	if( $('#imageFile').length != 0 ){

		var uploader = new plupload.Uploader({
			multi_selection : false, 
			runtimes : 'gears,html5,flash,silverlight,browserplus',
			browse_button : 'selectFile',
			//container: 'container',
			max_file_size : '10mb',
			url : 'assets/ajax_test/upload.php',
			// resize : {width : 320, height : 240, quality : 90},
			flash_swf_url : 'assets/js/library/plupload/plupload.flash.swf',
			silverlight_xap_url : 'assets/js/library/plupload/plupload.silverlight.xap',
			filters : [
				{title : "Image files", extensions : "jpg,gif,png"}
			]
		});
		
		uploader.bind('Init', function(up, files) {
		});
		
		// fires when files are added to queue
		uploader.bind('FilesAdded', function(up, files) {
			var thisImage = files[0];
			$('#imageFile').text( thisImage.name );
			$('.uploadWidget')
			.removeClass('selectState')
			.addClass('uploadState');
			
			newFile = thisImage.name;
			isFileSelected = true;
		});
		
		// fies as files are uploaded
		uploader.bind('UploadProgress', function(up, file) {
			var progressBar = Math.floor( file.percent * 182 );
			$('#imageProgress').css({ "width" : progressBar + "px" });
		});
	
		// fires when an indiviual file in queue completes upload
		uploader.bind('FileUploaded', function(up, file, r) {
			var fileUploads = $.parseJSON( r.response );
			newFile = fileUploads.filename;
			isFileUploaded = true;
			// fileUploads.filename
		});
		
		// fires when all files in queue have been uploaded
		uploader.bind('UploadComplete', function(up, file) {
			$('.uploadWidget')
			.removeClass('uploadState')
			.addClass('endState');
		});
		
		// fires when user clicks on 'upload' button - 
		$('#uploadFile').click(function(e){
			e.preventDefault();
			uploader.start();
		});
	}
	
	// top 10 image gallery rotator
	var ti = 1;
	// top10 view
	$('.lunchSpotGallery img').click(function(e){
		e.preventDefault();
		ti++;
		ti = (ti == 5) ? 1 : ti;
	});
	
	// top 10 'share' button
	function sharePlace( place ){
		// create new facebook post containing details of the lunch spots promotion and specific lunch spot
		console.log("share '%s'", currentPlace.title );
	}
	
/*
----------------------------------------------------------------------------------------------------------------
custom infoBox for maps
----------------------------------------------------------------------------------------------------------------
*/
	if( $('#googlePopupMap,#googleMap').length != 0 ){
		function init() {
			// Create info window. In content you can pass simple text or html code.
			var infowindow = new google.maps.InfoWindow({
				content: "",
				maxWidth: 10
			});

			var boxText = $('<div></div>');
			boxText
			.addClass('customInfoWindow')
			.html('');
			
			var myOptions1 = {
				content: boxText.html(),
				disableAutoPan: false,
				maxWidth: 0,
				pixelOffset: new google.maps.Size(-190, -150),
				zIndex: null,
				boxStyle: {},
				closeBoxMargin: "-10px -10px -23px 2px",
				closeBoxURL: "assets/img/prizes/close.png",
				infoBoxClearance: new google.maps.Size(1, 1),
				isHidden: false,
				pane: "floatPane",
				enableEventPropagation: false
			};
			
			ib = new InfoBox(myOptions1);

			// attaceh 'more' link event once infoBox HTMNL has been added to the DOM
			google.maps.event.addListener(ib, 'domready', function() {
				$(".infoBoxMore").click(function(e){
					e.preventDefault();

					// $('.infoBox').animate({ "height" : "350px" }, 500, function(){});
					$('.infoBox').css({ "height" : "auto" });
					$('.infoBox .scrollFrame')
					.removeClass('short')
					.addClass('long');
					
					$('.infoBox .scrollFrame').jScrollPane(
						{
							"verticalDragMinHeight" : 25,
							"verticalDragMaxHeight" : 25
						}
					);

				});
			});			
			
			google.maps.event.addListener(ib, 'closeclick', function() {
				currentMarker.setIcon( klsPin ); // change marker back to standard size
			});			
		}
		
		// Register an event listener to fire when the page finishes loading.
		google.maps.event.addDomListener(window, 'load', init);
	}

/*
----------------------------------------------------------------------------------------------------------------
map directions finder widget for top 10 list info overlay
----------------------------------------------------------------------------------------------------------------
*/
	var travelMode = google.maps.TravelMode.DRIVING;
	
	$('.travelMode li').click(function(e){
		e.preventDefault();
		// /tm[A-Z][a-z]+/
		
		var stateRegEx = /driving|transit|walking|cycling/;
		var matches = stateRegEx.exec( $('.travelMode').attr('class') );
		if( matches != null ){
			state = matches[0];
		}
		
		var travelRegEx = /TM(driving|transit|walking|cycling)/;
		var matches = travelRegEx.exec( $(this).attr('class') );
		if( matches != null ){
			action = matches[1];
			if( state != action ){
				$('.travelMode').removeClass( state );
				switch( action ){
					case 'driving': {
						$('.travelMode').addClass( 'driving' );
						travelMode = google.maps.TravelMode.DRIVING
					} break;
					case 'transit': {
						$('.travelMode').addClass( 'transit' );
						travelMode = google.maps.TravelMode.TRANSIT
					} break;
					case 'walking': {
						$('.travelMode').addClass( 'walking' );
						travelMode = google.maps.TravelMode.WALKING
					} break;
					case 'cycling': {
						$('.travelMode').addClass( 'cycling' );
						travelMode = google.maps.TravelMode.BICYCLING
					} break;
				}
			}
		}
		
	});

	// switch between map and directions panel
	$('.mapSwitch li a').click(function(e){
		e.preventDefault();
		var actionIndex = $('.mapSwitch li').index( $(this).parents('li').eq(0) );
		if( currentMapState != actionIndex ){
			switch( actionIndex ){
				case 0 : {
					// show map
					$('.mapSwitch')
					.removeClass('directionsState')
					.addClass('mapState');
					$('.directionsWidget').hide();
					$('.mapWidget').show();
				} break;
				case 1 : {
					// show directions
					$('.mapSwitch')
					.removeClass('mapState')
					.addClass('directionsState');
					$('.mapWidget').hide();
					$('.directionsWidget').show();
				} break;
			}
			currentMapState = actionIndex;
		}
	});
	
	// 'get directions' button pressed
	$('.getDirections').click(function(e){
		e.preventDefault();

		if( isNotBlank( $('#start').val() )  ){
			var request = {
				// "origin" : new google.maps.LatLng( start.lat, start.lng ),
				"origin" : $('#start').val(),
				"destination" : new google.maps.LatLng( currentPlace.lat, currentPlace.lng ),
				"travelMode" : travelMode
			};
	
			// execute request
			directionsService.route(request, function(result, status) {
				if (status == google.maps.DirectionsStatus.OK) {

					var steps = result.routes[0].legs[0].steps;
					$('#steps').empty();
					for( stepIndex = 0; stepIndex < steps.length; stepIndex++ ){
						var thisStep = steps[ stepIndex ];
	
						var xsf = thisStep.instructions.replace(/\<\//g,'TAGCLOSE');
						var slashes = xsf.replace(/\//g,' / ');
						slashes = slashes.replace(/TAGCLOSE/g,'</');

						var newStep = $('<li></li>');
						var newCell1 = $('<p></p>')
						.addClass('instruction')
						.html( (stepIndex + 1) + ' - ' + slashes );
						var newCell2 = $('<p></p>')
						.addClass('distance')
						.html( thisStep.distance.text );
						newStep
						.append( newCell1 )
						.append( newCell2 );
						$('#steps').append( newStep );
					}

					var modeText = '';
					switch ( travelMode ){
						case google.maps.TravelMode.DRIVING: {
							modeText = 'Driving';
						} break;
						case google.maps.TravelMode.TRANSIT : {
							modeText = 'Public Transport';
						} break;
						case google.maps.TravelMode.WALKING : {
							modeText = 'Walking';
						} break;
						case google.maps.TravelMode.BICYCLING : {
							modeText = 'Cycling';
						} break;
					}
					
					$('#mode').text( modeText );
					$('span#destination').text( currentPlace.title );
					$('.copyright').text( result.routes[0].copyrights );
					$('.totalDistance span').text( result.routes[0].legs[0].distance.text );
					$('.totalDuration span').text( result.routes[0].legs[0].duration.text );
					
					$('#directionsForm').animate({"opacity" : 0 }, 500, function(){
						$('#directionsForm').css({ "display" : "none" });
						$('#directionsListWrapper').css({ "display" : "block" });
						$('#directionsListWrapper').jScrollPane();
						$('#directionsListWrapper').animate({"opacity" : 1 }, 500, function(){
						});
					});
				}
			});
		}
	});
	
	// go back to directions form from results
	$('.tryAgain').click(function(e){
		e.preventDefault();
		$('#directionsListWrapper').animate({"opacity" : 0 }, 500, function(){
			$('#directionsListWrapper').css({ "display" : "none" });
			$('#steps').empty();
			$('#directionsForm').css({ "display" : "block" });
			$('#start').val('')
			$('#directionsForm').animate({"opacity" : 1 }, 500, function(){
			});
		});
	});
	
/*
----------------------------------------------------------------------------------------------------------------
google directions API example
----------------------------------------------------------------------------------------------------------------
*/
	if( $('#googleRectangle').length != 0 ){

		// create map instance in page in div#googleMap
		map = new google.maps.Map( document.getElementById("googleRectangle"), placePinOptions );

		// set up map directions overlay
		var directionsService = new google.maps.DirectionsService();
		var directionsDisplay = new google.maps.DirectionsRenderer();
		var end;
		
		// update map showing route from A to B
		directionsDisplay.setMap(map);

		// insert directions into specified DIV
		// directionsDisplay.setPanel(document.getElementById("directionsList"));

		end = places[ 5 ];
		$('.endPlace').text( end.title );

		/*
		var placeSelect = $('#end')
		var placeIndex, thisPlace, placeOption;
		for( placeIndex = 0; placeIndex < places.length; placeIndex++ ){
			thisPlace = places[ placeIndex ];
			placeOption = $('<option></option>')
			.attr({ "value" : placeIndex})
			.text( thisPlace.title );
			placeSelect.append( placeOption );
		}
		*/
		// $('.endPlace').text( end.title );

		insetMap( end );
	}
});
